<?
	require_once 'config.php';

    function get_user_os()
    {
        global $user_agent;

        $os_platform = 'Unknown OS';

        $os_array = array(
            '/windows nt 6.3/i' => 'Windows 8.1',
            '/windows nt 6.2/i' => 'Windows 8',
            '/windows nt 6.1/i' => 'Windows 7',
            '/windows nt 6.0/i' => 'Windows Vista',
            '/windows nt 5.2/i' => 'Windows Server 2003/XP x64',
            '/windows nt 5.1/i' => 'Windows XP',
            '/windows xp/i' => 'Windows XP',
            '/windows nt 5.0/i' => 'Windows 2000',
            '/windows me/i' => 'Windows ME',
            '/win98/i' => 'Windows 98',
            '/win95/i' => 'Windows 95',
            '/win16/i' => 'Windows 3.11',
            '/macintosh|mac os x/i' => 'Mac OS X',
            '/mac_powerpc/i' => 'Mac OS 9',
            '/linux/i' => 'Linux',
            '/ubuntu/i' => 'Ubuntu',
            '/iphone/i' => 'iPhone',
            '/ipod/i' => 'iPod',
            '/ipad/i' => 'iPad',
            '/android/i' => 'Android',
            '/blackberry/i' => 'BlackBerry',
            '/webos/i' => 'Mobile'
        );

        foreach($os_array as $regex => $value) {
            if(preg_match($regex, $user_agent)) {
                $os_platform = $value;
            }
        }

        return $os_platform;
    }

    function get_user_browser()
    {
        global $user_agent;

        $browser = 'Unknown Browser';

        $browser_array = array(
            '/msie/i' => 'Internet Explorer',
            '/firefox/i' => 'Firefox',
            '/safari/i' =>  'Safari',
            '/chrome/i' =>  'Chrome',
            '/opera/i' =>  'Opera',
            '/netscape/i' =>  'Netscape',
            '/maxthon/i' =>  'Maxthon',
            '/konqueror/i' =>  'Konqueror',
            '/mobile/i' =>  'Mobile Browser'
        );

        foreach($browser_array as $regex => $value) {
            if(preg_match($regex, $user_agent)) {
                $browser = $value;
            }
        }

        return $browser;
    }


    function store_tracking_stats()
    {
        global $db, $ip, $host_name, $date, $referrer, $user_agent;

        $os = get_user_os();
        $browser = get_user_browser();

        $stmt = $db->prepare("
            INSERT INTO
            stats(ip, host_name, browser, os, user_agent, visit_date, referrer)
            VALUES(?, ?, ?, ?, ?, ?, ?)
        ");
        $stmt->execute(array($ip, $host_name, $browser, $os, $user_agent, $date, $referrer));
    }

    function is_xlsx($finfo, $file)
    {
        $mime_type = finfo_file($finfo, $file);
        $file_type = explode('/', $mime_type);

        if($file_type[1] === 'vnd.ms-excel') {
            return true;
        } else {
            return false;
        }
    }

    function list_xlsx_files($finfo, $files)
    {
        $count = 0;

        foreach($files as $file) {
            if(is_file($file)) {
                if(is_xlsx($finfo, $file)) {
                    $count++;

                    echo '<div class="file-name" data-name="' . $file . '" data-number="' . $count . '">' . $file . '</div>';
                }
            }
        }
    }
?>