<?
	$page_title = '';

	require_once 'header.php';
?>

        <div id="files">
            <?
                $files = new RecursiveDirectoryIterator('xlsx/');
                $finfo = finfo_open(FILEINFO_MIME_TYPE);

                list_xlsx_files($finfo, $files);
                finfo_close($finfo);
            ?>
        </div>

        <form id="input-name" method="post" action="xlsx2csv.php">
            <label>XLXS File Name</label>
            <input id="file-name" type="text" name="input_name" placeholder="Full name of xlsx file to be converted" size="50">
            <input id="file-number" type="hidden">

            <button id="submit" type="submit">Convert</button>
        </form>

        <script src="<? echo $config['site']['root']; ?>/js/index.js"></script>

<?
	require_once 'footer.php';
?>