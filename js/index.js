$(document).on('click', '.file-name', function() {
    var file_name = $(this).data('name');
    var file_number = $(this).data('number');

    $('#file-name').val(file_name);
    $('#file-number').val(file_number);
});

$(document).on('submit', '#input-name', function(event) {
    var file_name = $('#file-name').val();
    var file_number = $('#file-number').val();
    var file_extension = file_name.split('.').pop();

    if(file_name.length > 0) {
        if(file_extension === 'xlsx') {
            $.ajax({
                type: 'POST',
                url: 'xlsx2csv.php',
                data: {
                    input_name: file_name
                },
                complete: function(data) {
                    $('[data-number="'+ file_number +'"]').addClass('converted');
                    console.log(data);
                }
            });
        } else {
            alert('Only xlsx files can be converted into CSV');
        }
    } else {
        alert('Please select a file name');
    }

    event.preventDefault();
    return false;
});